<?php

namespace Drupal\external_link_popup;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a external_link_popup entity.
 */
interface ExternalLinkPopupInterface extends ConfigEntityInterface {

}
